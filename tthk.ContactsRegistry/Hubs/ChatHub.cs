﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tthk.ContactsRegistry.Hubs
{
    public class ChatHub : Hub
    {
        public Task SendMessage(string groupName, string from, string message)
        {
            return Clients.Group(groupName)
                .SendAsync("ReceiveMessage", groupName, from, message);
        }

        public Task JoinGroup(string groupName)
        {
            return Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }
    }
}
