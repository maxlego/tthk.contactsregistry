import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as signalR from "@aspnet/signalr";

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public contacts: IContact[];
  public searchTerm: string = "";

  public messageA: string = "";
  public fromA: string = "";


  public messageB: string = "";
  public fromB: string = "";

  public messages: any = {}

  private readonly _hubConnection: signalR.HubConnection;

  constructor(private _http: HttpClient, @Inject('BASE_URL') private _baseUrl: string) {
    this._reloadList();

    this._hubConnection = new signalR.HubConnectionBuilder()
      .withUrl("/chatHub")
      .build();

    this._hubConnection.on("ReceiveMessage", (groupName: string, from: string, message: string) => {

      if (!this.messages[groupName]) {
        this.messages[groupName] = [];
      }

      this.messages[groupName].push({ from: from, message: message });

    });

    this._hubConnection
      .start()
      .then(() => console.log("connected to hub"))
      .catch(err => console.error(err));
  }

  public sendMessage(groupName: string, from: string, message: string) {
    this._hubConnection.send("SendMessage", groupName, from, message);
  }

  public joinGroup(groupName: string) {
    this._hubConnection.send("JoinGroup", groupName);
  }

  private _reloadList() {
    this._http.get<IContact[]>(this._baseUrl + 'api/Contacts', {
      params: {
        term: this.searchTerm
      }
    })
      .subscribe(result => {
        this.contacts = result;
      }, error => console.error(error));
  }

  public delete(contact: IContact, i: number) {
    this._http.delete(this._baseUrl + `api/Contacts/${contact.id}`)
      .subscribe(result => {
        this.contacts.splice(i, 1);
      }, error => console.error(error));
  }

  public search() {
    this._reloadList();
  }
}

interface IContact {
  id: string,
  name: string;
  defaultPhoneNumber: string;
  defaultEmail: string;
}
